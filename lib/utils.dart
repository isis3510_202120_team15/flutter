import 'dart:async';
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  final swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}

class MyConnectivity {
  Future<AddressCheckResult> isHostReachable(
    AddressCheckOptions options,
  ) async {
    Socket sock;
    try {
      sock = await Socket.connect(
        options.address,
        options.port,
        timeout: options.timeout,
      );
      sock.destroy();
      return AddressCheckResult(options, true);
    } catch (e) {
      return AddressCheckResult(options, false);
    }
  }

  static final List<AddressCheckOptions> DEFAULT_ADDRESSES = List.unmodifiable([
    AddressCheckOptions(
      InternetAddress('1.1.1.1'),
      port: 53,
      timeout: const Duration(seconds: 10),
    ),
    AddressCheckOptions(
      InternetAddress('8.8.4.4'),
      port: 53,
      timeout: const Duration(seconds: 10),
    ),
    AddressCheckOptions(
      InternetAddress('208.67.222.222'),
      port: 53,
      timeout: const Duration(seconds: 10),
    ),
  ]);
  MyConnectivity._();

  static final _instance = MyConnectivity._();
  static MyConnectivity get instance => _instance;
  var _controller = StreamController.broadcast();
  var _connectivity = Connectivity();
  Stream get myStream => _controller.stream;
  List<AddressCheckOptions> addresses = DEFAULT_ADDRESSES;
  List<AddressCheckResult> get lastTryResults => _lastTryResults;
  List<AddressCheckResult> _lastTryResults = <AddressCheckResult>[];
  void initialize() async {
    _checkStatus();
    _connectivity.onConnectivityChanged.listen((result) {
      _checkStatus();
    });
  }

  void _checkStatus() async {
    List<Future<AddressCheckResult>> requests = [];

    for (var addressOptions in addresses) {
      requests.add(isHostReachable(addressOptions));
    }
    _lastTryResults = List.unmodifiable(await Future.wait(requests));

    _controller.sink
        .add(lastTryResults.map((result) => result.isSuccess).contains(true));
  }

  void disposeStream() => {_controller.close()};
}

class AddressCheckOptions {
  final InternetAddress address;
  final int port;
  final Duration timeout;

  AddressCheckOptions(
    this.address, {
    this.port = 53,
    this.timeout = const Duration(seconds: 10),
  });

  @override
  String toString() => "AddressCheckOptions($address, $port, $timeout)";
}

class AddressCheckResult {
  final AddressCheckOptions options;
  final bool isSuccess;

  AddressCheckResult(
    this.options,
    this.isSuccess,
  );

  @override
  String toString() => "AddressCheckResult($options, $isSuccess)";
}

class NoCachedInformation implements Exception {
  String cause;
  NoCachedInformation(this.cause);
}
