import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:reservations_app/models/plate.dart';

class PlateDetail extends StatefulWidget {
  final bool menuActive;
  final bool ttgOrder;
  final ValueChanged parentAction;
  final List<Plate> plates;
  PlateDetail(
      {Key? key,
      required this.menuActive,
      required this.parentAction,
      required this.plates,
      required this.ttgOrder})
      : super(key: key);

  @override
  _PlateState createState() => _PlateState();
}

class _PlateState extends State<PlateDetail> {
  static const double _hPad = 16.0;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          padding: const EdgeInsets.fromLTRB(_hPad, 32.0, _hPad, 4.0),
          child: SizedBox(
              height: 600,
              child: ListView.separated(
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                  padding: const EdgeInsets.all(8),
                  itemCount: widget.plates.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _rowBuilder(widget.plates[index]);
                  })),
        )
      ],
    ));
  }

  Widget _rowBuilder(Plate plate) {
    return Stack(
      alignment: AlignmentDirectional.bottomEnd,
      children: [
        Card(
          elevation: 5,
          color: Color(0xff999990),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
          ),
          child: ListTile(
            contentPadding: const EdgeInsets.fromLTRB(10.0, 7.0, 10.0, 7.0),
            leading: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: Image.asset("assets/images/borgar.jpg")),
            title: Text(plate.nombre.toString(),
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
            trailing: Text(
              r"$" + plate.precio.toString(),
              style: TextStyle(
                  color: Color(0xffba1200),
                  fontWeight: FontWeight.bold,
                  fontSize: 13,
                  fontFamily: "Segoe UI"),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        ConstrainedBox(
          constraints: BoxConstraints.tightFor(width: 30, height: 30),
          child: ElevatedButton(
              onPressed: () {
                if (widget.ttgOrder) {
                  setState(() {
                    widget.parentAction(plate);
                  });
                } else {
                  if (widget.menuActive) {
                    setState(() {
                      widget.parentAction(plate);
                    });
                  } else {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => AlertDialog(
                              title: Text("Attention"),
                              content: Text(
                                  "You must select a reservation hour before adding items to your cart."),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () => Navigator.pop(context, 'Ok'),
                                  child: const Text('Ok'),
                                ),
                              ],
                            ));
                  }
                }
              },
              child: const Center(child: Icon(Icons.add, size: 15)),
              style: ElevatedButton.styleFrom(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  elevation: 40,
                  shape: CircleBorder(),
                  primary: Color(0xff031927),
                  alignment: Alignment.centerLeft)),
        )
      ],
    );
  }
}
