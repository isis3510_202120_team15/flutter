import 'package:flutter/material.dart';

class MyStatefulText extends StatefulWidget {
  const MyStatefulText({Key? key, required this.myValue}) : super(key: key);
  final String myValue;

  @override
  _MyStatefulTextState createState() => _MyStatefulTextState();
}

class _MyStatefulTextState extends State<MyStatefulText> {
  String localValue = "";

  @override
  void initState() {
    localValue = widget.myValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Text(localValue);
  }
}
