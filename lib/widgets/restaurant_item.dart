import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:reservations_app/models/restaurant.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:reservations_app/restaurant_detail.dart';
import '../services/restaurant_service.dart';

class RestaurantItem extends StatefulWidget {
  final Restaurant restaurant;
  final double latitude;
  final double longitude;
  final bool isOnline;

  RestaurantItem(this.restaurant, this.latitude, this.longitude, this.isOnline);

  @override
  State<RestaurantItem> createState() => _RestaurantItemState();
}

class _RestaurantItemState extends State<RestaurantItem> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(widget.restaurant.nombre as String),
      leading: CachedNetworkImage(
        width: 50,
        height: 50,
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
        placeholder: (context, url) => CircularProgressIndicator(),
        imageUrl: widget.restaurant.foto as String,
      ),
      subtitle: RatingBarIndicator(
        rating: widget.restaurant.getRating(),
        itemBuilder: (context, index) => Icon(
          Icons.star,
          color: Colors.amber,
        ),
        itemCount: 5,
        itemSize: 15,
      ),
      onTap: () {
        Navigator.of(context).pushNamed(RestaurantDetail.routeName,
            arguments: {'restaurant': widget.restaurant});
        return;
      },
      trailing: FutureBuilder(
          future: getRestaurantLocation(
            widget.restaurant,
            widget.latitude,
            widget.longitude,
            widget.isOnline
          ),
          builder: (ctx, snapshot) {
            if (snapshot.hasData) {
              if ((snapshot.data as List)[2] == '') {
                return Text("");
              } else {
                return Container(
                  width: MediaQuery.of(context).size.width * 0.20,
                  child: Row(
                    children: [
                      Text((snapshot.data as List)[2] as String,
                          style: TextStyle(fontSize: 13)),
                      Icon(
                        Icons.location_on,
                        color: Color(0xffba1200),
                      ),
                    ],
                  ),
                );
              }
            } else
              return Text("");
          }),
    );
  }
}
