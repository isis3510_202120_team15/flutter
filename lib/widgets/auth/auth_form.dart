import 'package:flutter/material.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:connectivity/connectivity.dart';

class AuthForm extends StatefulWidget {
  AuthForm(this.submitFn, this.isLoading);
  final void Function(
      String email,
      String password,
      String username,
      String phone,
      String documentType,
      String document,
      bool isLogin,
      BuildContext context) submitFn;
  final bool isLoading;

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  final _formKey = GlobalKey<FormState>();
  var _isLogin = true;
  String userEmail = '';
  String userName = '';
  String userPassword = '';
  String userPhone = '';
  String userDocumentType = '';
  String userDocument = '';
  final Connectivity _connectivity = Connectivity();
  ConnectivityResult? connectionStatus;

  void _trySubmit() {
    final isValid = _formKey.currentState!.validate();
    FocusScope.of(context).unfocus();
    if (isValid) {
      _formKey.currentState!.save();
      widget.submitFn(
          userEmail.trim(),
          userPassword.trim(),
          userName.trim(),
          userPhone.trim(),
          userDocumentType.trim(),
          userDocument.trim(),
          _isLogin,
          context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(16),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextFormField(
                    key: ValueKey('email'),
                    validator: (value) {
                      if (value!.isEmpty || !value.contains('@')) {
                        return 'Enter a valid email address';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                      labelText: 'Email Adress',
                    ),
                    onSaved: (value) {
                      userEmail = value!;
                    },
                  ),
                  if (!_isLogin)
                    TextFormField(
                      key: ValueKey('username'),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Enter a valid username';
                        }
                        return null;
                      },
                      decoration: InputDecoration(labelText: 'Name'),
                      onSaved: (value) {
                        userName = value!;
                      },
                    ),
                  if (!_isLogin)
                    TextFormField(
                      key: ValueKey('phone'),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Enter a valid phone';
                        }
                        return null;
                      },
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(labelText: 'Phone number'),
                      onSaved: (value) {
                        userPhone = value!;
                      },
                    ),
                  if (!_isLogin)
                    SelectFormField(
                      key: ValueKey('documentType'),
                      type: SelectFormFieldType.dropdown,
                      initialValue: 'C.C',
                      labelText: 'Document type',
                      items: [
                        {'value': 'T.I', 'label': 'T.I.'},
                        {'value': 'C.C', 'label': 'C.C.'},
                        {'value': 'C.E', 'label': 'C.E.'},
                      ],
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Enter a valid Document type';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        userDocumentType = value!;
                      },
                    ),
                  if (!_isLogin)
                    TextFormField(
                      key: ValueKey('document'),
                      validator: (value) {
                        if (value!.isEmpty || value.length < 6) {
                          return 'Enter a valid document';
                        }
                        return null;
                      },
                      decoration: InputDecoration(labelText: 'Document'),
                      onSaved: (value) {
                        userDocument = value!;
                      },
                    ),
                  TextFormField(
                    key: ValueKey('password'),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Enter a valid password';
                      }
                      return null;
                    },
                    decoration: InputDecoration(labelText: 'Password'),
                    obscureText: true,
                    onSaved: (value) {
                      userPassword = value!;
                    },
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  if (widget.isLoading) CircularProgressIndicator(),
                  if (!widget.isLoading)
                    ElevatedButton(
                      onPressed: () async {
                        connectionStatus =
                            await _connectivity.checkConnectivity();
                        switch (connectionStatus) {
                          case ConnectivityResult.wifi:
                          case ConnectivityResult.mobile:
                            _trySubmit();
                            return;
                          case ConnectivityResult.none:
                          case null:
                            showDialog(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                      title: Text("Attention"),
                                      content: Text(
                                          "You must have internet connection to authenticate"),
                                      actions: <Widget>[
                                        TextButton(
                                          onPressed: () =>
                                              Navigator.pop(context, 'Ok'),
                                          child: const Text('Ok'),
                                        ),
                                      ],
                                    ));
                            return;
                        }
                      },
                      child: Text(_isLogin ? 'Login' : 'Sign up'),
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20),
                      )),
                    ),
                  TextButton(
                      onPressed: () {
                        setState(() {
                          _isLogin = !_isLogin;
                        });
                      },
                      child: Text(_isLogin
                          ? 'Create an account'
                          : 'I already have an account')),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
