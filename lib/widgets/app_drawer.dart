import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:reservations_app/screens/ttg_order_detail.dart';
import '../screens/restaurants_screen.dart';

class AppDrawer extends StatefulWidget {
  @override
  State<AppDrawer> createState() => _AppDrawerState();
}

class _AppDrawerState extends State<AppDrawer> {
  String text = 'Hello friend!';

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            title: Text(text),
            automaticallyImplyLeading: false,
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.local_restaurant),
            title: Text('Restaurants'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed('/');
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.payment),
            title: Text('Orders'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => TtgOrderDetail()));
            },
          ),
          Divider(),
          ListTile(
              leading: Icon(Icons.account_balance_wallet),
              title: Text('Credit cards'),
              onTap: () {
                Navigator.of(context).pushReplacementNamed('/credit-cards');
              }),
          Divider(),
          ListTile(
            leading: Icon(Icons.add_box),
            title: Text('Add credit card'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed('/add-credit-card');
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Logout'),
            onTap: () {
              FirebaseAuth.instance.signOut();
            },
          ),
        ],
      ),
    );
  }
}
