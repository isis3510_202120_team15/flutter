class Plate {
  String? id;
  String? nombre;
  List<String>? categorias;
  int? precio;

  Plate(this.nombre, this.categorias, this.precio);

  Plate.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    categorias =
        json['categorias'] == null ? null : json['categorias'].cast<String>();
    precio = json['precio'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nombre'] = this.nombre;
    data['categorias'] = this.categorias;
    data['precio'] = this.precio;
    return data;
  }
}
