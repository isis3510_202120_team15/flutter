import '/models/ingredient.dart';
import '/models/order.dart';
import '/models/plate.dart';

class Restaurant {
  String? id;
  String? nombre;
  String? correo;
  String? telefono;
  String? nit;
  String? direccion;
  List<Order>? pedidos;
  List<Plate>? platos;
  List<Ingredient>? ingredientes;
  List<String>? horariosReserva;
  int? puestosReserva;
  String? disponibleTTG;
  List<int>? rating;
  String? foto;
  String distance = "";

  Restaurant(
      this.id,
      this.nombre,
      this.correo,
      this.telefono,
      this.nit,
      this.direccion,
      this.pedidos,
      this.platos,
      this.ingredientes,
      this.horariosReserva,
      this.puestosReserva,
      this.disponibleTTG,
      this.rating,
      this.foto,
      this.distance);

  Restaurant.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nombre = json['nombre'];
    correo = json['correo'];
    telefono = json['telefono'];
    nit = json['nit'];
    direccion = json['direccion'];
    if (json['pedidos'] != null) {
      pedidos = [];
      json['pedidos'].forEach((v) {
        pedidos!.add(new Order.fromJson(v));
      });
    }
    if (json['platos'] != null) {
      platos = [];
      json['platos'].forEach((v) {
        platos!.add(new Plate.fromJson(v));
      });
    }
    if (json['ingredientes'] != null) {
      ingredientes = [];
      json['ingredientes'].forEach((v) {
        ingredientes!.add(new Ingredient.fromJson(v));
      });
    }
    horariosReserva = json['horariosReserva'].cast<String>();
    puestosReserva = json['puestosReserva'];
    disponibleTTG = json['disponibleTTG'];
    foto = json['foto'];
    rating = json['rating'].cast<int>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['nombre'] = this.nombre;
    data['correo'] = this.correo;
    data['telefono'] = this.telefono;
    data['nit'] = this.nit;
    data['direccion'] = this.direccion;
    if (this.pedidos != null) {
      data['pedidos'] = this.pedidos!.map((v) => v.toJson()).toList();
    }
    if (this.platos != null) {
      data['platos'] = this.platos!.map((v) => v.toJson()).toList();
    }
    if (this.ingredientes != null) {
      data['ingredientes'] = this.ingredientes!.map((v) => v.toJson()).toList();
    }
    data['horariosReserva'] = this.horariosReserva;
    data['puestosReserva'] = this.puestosReserva;
    data['disponibleTTG'] = this.disponibleTTG;
    data['rating'] = this.rating;
    data['foto'] = this.foto;
    return data;
  }

  double getRating() {
    int rating = 0;
    for (var item in this.rating as List) {
      rating += item as int;
    }
    return rating / (this.rating as List).length;
  }
}
