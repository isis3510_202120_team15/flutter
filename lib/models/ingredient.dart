class Ingredient {
  String? categoria;
  String? nombre;

  Ingredient({required this.categoria, required this.nombre});

  Ingredient.fromJson(Map<String, dynamic> json) {
    categoria = json['categoria'];
    nombre = json['nombre'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['categoria'] = this.categoria;
    data['nombre'] = this.nombre;
    return data;
  }
}