class Card {
  String? cardHolder;
  String? cardNumber;
  String? fecha;
  String? id;

  Card({this.cardHolder, this.cardNumber, this.fecha, this.id});

  Card.fromJson(Map<String, dynamic> json) {
    cardHolder = json['cardHolder'];
    cardNumber = json['cardNumber'];
    fecha = json['fecha'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cardHolder'] = this.cardHolder;
    data['cardNumber'] = this.cardNumber;
    data['fecha'] = this.fecha;
    data['id'] = this.id;
    return data;
  }
}