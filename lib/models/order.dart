import 'dart:convert';

class Order {
  String? clienteId;
  String? restauranteId;
  List<String>? platos;
  String? diaReserva;
  String? horaReserva;
  int? puestos;

  Order(this.clienteId, this.restauranteId, this.platos, this.diaReserva,
      this.horaReserva, this.puestos);

  Order.fromJson(Map<String, dynamic> json) {
    clienteId = json['clienteId'];
    restauranteId = json['restauranteId'];
    if (json['platos'] != null) {
      platos = [];
      json['platos'].forEach((v) {
        platos!.add(v);
      });
    }
    diaReserva = json['diaReserva'];
    horaReserva = json['horaReserva'];
    puestos = json['puestos'];
  }

  Order.fromJsonTtg(Map<String, dynamic> json) {
    clienteId = json['clienteId'];
    restauranteId = json['restauranteId'];
    if (json['platos'] != null) {
      platos = [];
      json['platos'].forEach((v) {
        platos!.add(v);
      });
    }
    diaReserva = json['diaPedido'];
    horaReserva = json['horaPedido'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['clienteId'] = this.clienteId;
    data['restauranteId'] = this.restauranteId;
    if (this.platos != null) {
      data['platos'] = this.platos!.map((v) => jsonEncode(v)).toList();
    }
    data['diaReserva'] = this.diaReserva;
    data['horaReserva'] = this.horaReserva;
    data['puestos'] = this.puestos;
    return data;
  }

  Map<String, dynamic> toJsonTtg() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['clienteId'] = this.clienteId;
    data['restauranteId'] = this.restauranteId;
    if (this.platos != null) {
      data['platos'] = this.platos!.map((v) => jsonEncode(v)).toList();
    }
    data['diaPedido'] = this.diaReserva;
    data['horaPedido'] = this.horaReserva;
    return data;
  }
}
