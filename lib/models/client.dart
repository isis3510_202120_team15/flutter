class Client {
  String? id;
  String? name;
  String? email;
  String? phone;
  String? documentType;
  String? document;
  List? cards;
  List? orders;
  Client(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.documentType,
      this.document,
      this.cards,
      this.orders});
  Client.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['nombre'];
    email = json['correo'];
    phone = json['telefono'];
    documentType = json['tipoDoc'];
    document = json['document'];
    cards = json['tarjetas'];
    orders = json['pedidos'];
  }

  Map toJson() => {
        'id': id,
        'nombre': name,
        'correo': email,
        'telefono': phone,
        'tipoDoc': documentType,
        'document': document,
        'tarjetas': cards,
        'pedidos': orders,
      };
}
