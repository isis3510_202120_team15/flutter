import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:reservations_app/cart.dart';
import 'package:reservations_app/plate_detail.dart';
import 'package:reservations_app/widgets/my_stateful_text.dart';
import '/models/restaurant.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:percent_indicator/percent_indicator.dart';

var boxDecoration = BoxDecoration(
    border: Border.all(color: Colors.grey),
    borderRadius: BorderRadius.all(Radius.circular(30)),
    color: Color(0xfffcfff7),
    boxShadow: kElevationToShadow[3]);

class RestaurantDetail extends StatefulWidget {
  static const routeName = '/restaurant-detail';
  @override
  _RestarurantState createState() => _RestarurantState();
}

Map<int, int> _getAvailabilitiesForToday(Restaurant restaurant) {
  var today = new DateTime.utc(2021, 10, 20);
  Map<int, int> map = new Map();
  DateTime parsedDate;
  restaurant.pedidos!.forEach((e) => {
        if (e.horaReserva != null)
          {
            parsedDate = DateTime.parse(e.horaReserva.toString()),
            if (parsedDate.year == today.year &&
                parsedDate.month == today.month &&
                parsedDate.day == today.day)
              {
                if (map.containsKey(parsedDate.hour + parsedDate.minute))
                  {
                    map[parsedDate.hour + parsedDate.minute] =
                        map[parsedDate.hour + parsedDate.minute]! + 1
                  }
                else
                  {map[parsedDate.hour + parsedDate.minute] = 1}
              }
          }
      });
  return map;
}

class _RestarurantState extends State<RestaurantDetail> {
  var selectedReservationHour = "";
  static const double _hPad = 15.0;
  var selectedReservations = Set();
  int? cantPuestos = 0;
  var _cartList = Map();
  var total = 0;
  bool orderType = true;
  List<bool> _isSelected = [false, true];

  bool ttgOrderSelect = false;

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;

    var currentCart = Cart(
      selectedReservationHour: selectedReservationHour,
      cartList: _cartList,
      addCount: _addCartCounter,
      substractCount: _substractCartCounter,
      restauranteId: arguments['restaurant'].id,
      cantPuestos: cantPuestos,
    );
    var restaurant = arguments['restaurant'];
    var availabilities = _getAvailabilitiesForToday(restaurant!);
    var totalAvailability = restaurant.puestosReserva;
    var numReservations = restaurant.horariosReserva!.length;
    return Scaffold(
        floatingActionButtonAnimator: FloatingActionButtonAnimator.scaling,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: Visibility(
          visible: _cartList.length > 0,
          child: FloatingActionButton.extended(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => currentCart));
            },
            label: Text("Go to checkout"),
            icon: Icon(Icons.shopping_cart),
            backgroundColor: Color(0xffba1200),
            elevation: 50,
            hoverElevation: 70,
            focusElevation: 70,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
        ),
        appBar: AppBar(
          title: Text("Restaurant details"),
          actions: [
            Stack(
              alignment: Alignment.centerLeft,
              children: <Widget>[
                IconButton(
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => currentCart));
                    },
                    icon: Icon(
                      Icons.shopping_cart,
                      size: 36.0,
                    )),
                if (_cartList.length > 0)
                  Padding(
                    padding: const EdgeInsets.only(left: 2.0),
                    child: CircleAvatar(
                      radius: 8.0,
                      backgroundColor: Color(0xff031927),
                      foregroundColor: Colors.white,
                      child: Text(
                        _cartList.values.reduce((a, b) => a + b).toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12.0,
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ],
        ),
        body: Container(
            padding: const EdgeInsets.fromLTRB(_hPad, 32.0, _hPad, 4.0),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(
                      width: 150,
                      height: 150,
                      child: CachedNetworkImage(
                        width: 50,
                        height: 50,
                        imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        imageUrl: restaurant.foto as String,
                      )),
                  Container(
                    padding:
                        const EdgeInsets.fromLTRB(_hPad, 16.0, _hPad, 10.0),
                    child: Center(
                        child: Text(
                      restaurant.nombre.toString(),
                      style: TextStyle(
                          fontSize: 18.0, fontWeight: FontWeight.bold),
                    )),
                  ),
                  Container(
                    margin: const EdgeInsets.all(10.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ToggleButtons(
                          borderRadius: BorderRadius.circular(20),
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: Text("Take to Go Order"),
                            ),
                            Container(
                              margin: const EdgeInsets.all(10.0),
                              child: Text("Reservation with Order"),
                            ),
                          ],
                          onPressed: (int index) {
                            setState(() {
                              for (int buttonIndex = 0;
                                  buttonIndex < _isSelected.length;
                                  buttonIndex++) {
                                if (buttonIndex == index) {
                                  _isSelected[buttonIndex] = true;
                                } else {
                                  _isSelected[buttonIndex] = false;
                                }
                              }
                              if (index == 0) {
                                ttgOrderSelect = true;
                                cantPuestos = null;
                                DateTime now = new DateTime.now();

                                var formatterHour = new DateFormat('HH:mm');
                                selectedReservationHour =
                                    formatterHour.format(now);
                                print(selectedReservationHour);
                                //Setear hora actual en el carrito
                                //Puestos en null

                              } else if (index == 1) {
                                ttgOrderSelect = false;
                                cantPuestos = 0;
                                //Setear puestos en un número y reestablecer la hora a null
                              }
                            });
                          },
                          isSelected: _isSelected,
                        ),
                      ],
                    ),
                  ),
                  !ttgOrderSelect
                      ? _displayRestaurantAvailability(
                          restaurant, numReservations)
                      : Container(),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: Column(
                      children: [
                        Center(
                          child: Text("Menu",
                              style: TextStyle(
                                  fontSize: 18.0, fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 640,
                          child: PlateDetail(
                            menuActive: selectedReservationHour != "",
                            parentAction: _addCartCounter,
                            plates: restaurant.platos,
                            ttgOrder: ttgOrderSelect,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )));
  }

  Widget _displayRestaurantAvailability(
      Restaurant restaurant, int numReservations) {
    var availabilities = _getAvailabilitiesForToday(restaurant);
    var totalAvailability = restaurant.puestosReserva;

    print("Entró a Reservation with Order");

    return Container(
        decoration: boxDecoration,
        padding: const EdgeInsets.fromLTRB(10.0, 7.0, _hPad, 10.0),
        child: Column(
          children: [
            Center(
              child: Text(
                "Today's Availability",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Center(
              child: Icon(
                Icons.access_time_filled,
                color: Colors.amber,
                size: 35,
              ),
            ),
            _addPuestosWidget(),
            SizedBox(
              height: 200,
              child: ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: restaurant.horariosReserva!.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (index < numReservations - 1) {
                      print("Está poniendo las líneas");
                      return _rowBuilder(
                          restaurant.horariosReserva![index],
                          restaurant.horariosReserva![index + 1],
                          availabilities,
                          totalAvailability!.toInt());
                    }
                    return SizedBox();
                  }),
            )
          ],
        ));
  }

  _addCartCounter(plate) {
    setState(() {
      if (_cartList.containsKey(plate)) {
        _cartList[plate] = _cartList[plate]! + 1;
      } else {
        _cartList[plate] = 1;
      }
    });
  }

  _substractCartCounter(plate) {
    setState(() {
      if (_cartList.containsKey(plate)) {
        if (_cartList[plate] == 1) {
          _cartList.remove(plate);
        } else {
          _cartList[plate] = _cartList[plate]! - 1;
        }
      }
    });
  }

  Widget _rowBuilder(String startDate, String endDate,
      Map<int, int> availabilities, int total) {
    var count = 0;
    var startDateSum =
        startDate.split(":").map((e) => int.parse(e)).reduce((a, b) => a + b);
    if (availabilities.containsKey(startDateSum)) {
      count = availabilities[startDateSum] as int;
    }
    return Container(
        padding: const EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Center(
                child: HourDetail(
              reserved: selectedReservationHour != "",
              startDate: startDate,
              endDate: endDate,
              selectedReservations: selectedReservations,
            )),
            LinearPercentIndicator(
              width: MediaQuery.of(context).size.width * 0.3,
              percent: count / total,
              progressColor: Color(0xff031927),
              animation: true,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Stack(
                children: [
                  Positioned.fill(
                    child: Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: <Color>[
                            Color(0xffba1200),
                            Color(0xffd51200),
                            Color(0xfff11200),
                          ],
                        ),
                      ),
                    ),
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      primary: Colors.white,
                    ),
                    onPressed: count == total
                        ? null
                        : () {
                            setState(() {
                              if (selectedReservationHour == startDate) {
                                selectedReservationHour = '';
                                selectedReservations.clear();
                              } else {
                                selectedReservationHour = startDate;
                                if (selectedReservations.length == 0) {
                                  selectedReservations.add(startDate);
                                } else {
                                  selectedReservations.clear();
                                  selectedReservations.add(startDate);
                                }
                              }
                            });
                          },
                    child: const Text(
                      'Reserve',
                      style: TextStyle(fontSize: 14.0, fontFamily: "Segoe UI"),
                    ),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  _addPuestosWidget() {
    return Wrap(
        spacing: 12,
        crossAxisAlignment: WrapCrossAlignment.center,
        alignment: WrapAlignment.center,
        children: <Widget>[
          GestureDetector(
            child: Icon(
              Icons.remove_circle,
              color: Colors.red,
              size: 30,
            ),
            onTap: () {
              setState(() {
                if (cantPuestos != null) {
                  cantPuestos = cantPuestos! - 1;
                }
              });
            },
          ),
          MyStatefulText(myValue: cantPuestos.toString(), key: UniqueKey()),
          GestureDetector(
            child: Icon(
              Icons.add_circle,
              color: Color(0xFF031927),
              size: 30,
            ),
            onTap: () {
              setState(() {
                if (cantPuestos != null) {
                  cantPuestos = cantPuestos! + 1;
                }
              });
              // Add item from cart and update restaurant detail screen
            },
          ),
        ]);
  }
}

class HourDetail extends StatefulWidget {
  final bool reserved;
  final String startDate;
  final String endDate;
  final Set selectedReservations;
  HourDetail(
      {Key? key,
      required this.reserved,
      required this.startDate,
      required this.endDate,
      required this.selectedReservations})
      : super(key: key);

  @override
  _HourDetailState createState() => _HourDetailState();
}

class _HourDetailState extends State<HourDetail> {
  @override
  Widget build(BuildContext context) {
    return Container(
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(
            border: widget.selectedReservations.contains(widget.startDate)
                ? Border.all(color: Color(0xffffaa00))
                : Border.all(color: Colors.grey),
            borderRadius: BorderRadius.all(Radius.circular(30)),
            color: widget.selectedReservations.contains(widget.startDate)
                ? Color(0xffffaa00)
                : Color(0xfffcfff7),
            boxShadow: kElevationToShadow[3]),
        padding: const EdgeInsets.fromLTRB(10.0, 7.0, 10.0, 7.0),
        width: 110,
        child: Text(
          widget.startDate + " - " + widget.endDate,
          style: TextStyle(fontFamily: "Segoe UI"),
        ));
  }
}
