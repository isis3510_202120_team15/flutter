import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:reservations_app/restaurant_detail.dart';
import 'package:reservations_app/screens/auth_screen.dart';
import 'package:reservations_app/screens/credit_cards.dart';
import 'package:reservations_app/utils.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:path_provider/path_provider.dart';

import 'screens/restaurants_screen.dart';
import 'screens/add_credit_card_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final _navigatorKey = new GlobalKey<NavigatorState>();
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);
  @override
  Widget build(BuildContext context) {
    final ThemeData theme = ThemeData();
    return MultiProvider(
      providers: [
        Provider<FirebaseAnalytics>.value(value: analytics),
        Provider<FirebaseAnalyticsObserver>.value(value: observer),
      ],
      child: MaterialApp(
        title: 'Reservations',
        navigatorKey: _navigatorKey,
        theme: ThemeData(
            colorScheme: theme.colorScheme.copyWith(
                primary: createMaterialColor(Color(0xffba1200)),
                secondary: Color(0xff031927),
                background: Color(0xfffcfff7)),
            fontFamily: 'Poppins',
            buttonTheme: ButtonTheme.of(context).copyWith(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)))),
        routes: {
          RestaurantDetail.routeName: (ctx) => RestaurantDetail(),
          RestaurantsScreen.routeName: (ctx) => RestaurantsScreen(),
          AuthScreen.routeName: (ctx) => AuthScreen(),
          CardsScreen.routeName: (ctx) => CardsScreen(),
          AddCreditCardScreen.routeName: (ctx) => AddCreditCardScreen(),
        },
        home: FutureBuilder(
            future: Firebase.initializeApp(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                FirebaseAuth.instance.authStateChanges().listen((User? user) {
                  if (user == null)
                    _navigatorKey.currentState!
                        .pushReplacementNamed(AuthScreen.routeName);
                  else
                    _navigatorKey.currentState!
                        .pushReplacementNamed(RestaurantsScreen.routeName);
                });
              } else if (snapshot.hasError) {
                print("no connection");
              }
              return MaterialApp(
                home: Scaffold(
                  body: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}
