import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:reservations_app/models/client.dart';
import 'package:reservations_app/models/order.dart';
import 'package:reservations_app/models/restaurant.dart';
import 'package:reservations_app/services/client_service.dart';
import 'package:reservations_app/services/order_service.dart';
import 'package:reservations_app/services/restaurant_service.dart';

class TtgOrderDetail extends StatefulWidget {
  static const routeName = '/order-detail';
  // final Map plateList;
  // final String restauranteId;
  // final String horaPedido;
  // final String horaEstimada;

  // const ttgOrderDetail(
  //     {Key? key,
  //     required this.plateList,
  //     required this.restauranteId,
  //     required this.horaPedido,
  //     required this.horaEstimada})
  //     : super(key: key);

  @override
  _TtgOrderState createState() => _TtgOrderState();
}

class _TtgOrderState extends State<TtgOrderDetail> {
  List<dynamic> orderList = [];

  Future<dynamic> getClientId() async {
    Future client = getCurrentClient();
    return client;
  }

  bool _fileExists = false;

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/clientOrders.json');
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getClientId(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            Client client = snapshot.data as Client;
            Future<File> file = _localFile;
            return FutureBuilder(
                future: getTtgOrderListByClient(client.id!),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    orderList = snapshot.data as List<dynamic>;
                    print(orderList.toString());

                    for (Order order in orderList) {
                      print(order.horaReserva);
                    }

                    return Scaffold(
                      appBar: AppBar(
                        title: const Text("Take to Go Orders"),
                      ),
                      body: ListView.builder(
                        itemCount: orderList.length,
                        itemBuilder: (context, index) {
                          if (orderList.isEmpty) {
                            return Text(
                                "There are no orders yet, take a look at our best restaurant offers and place your first order!");
                          }
                          return FutureBuilder(
                              future: getRestaurantById(
                                  orderList[index].restauranteId),
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  Restaurant res = snapshot.data as Restaurant;
                                  //Order State: 0=Cancelled; 1=Ready at:; 2=Delivered
                                  String orderState;
                                  String diaReserva =
                                      orderList[index].diaReserva;
                                  String horaReserva =
                                      orderList[index].horaReserva;

                                  DateTime now = new DateTime.now();

                                  var formatterHour = new DateFormat('HH:mm');
                                  String horaActual = formatterHour.format(now);

                                  var formatterDay =
                                      new DateFormat('dd/MM/yyyy');
                                  String diaActual = formatterDay.format(now);

                                  String horaPickup;

                                  horaPickup = horaReserva.split(":")[0] == "23"
                                      ? "00:" + horaReserva.split(":")[1]
                                      : (int.parse(horaReserva.split(":")[0]) +
                                                  1)
                                              .toString() +
                                          ":" +
                                          horaReserva.split(":")[1];

                                  if (diaReserva.compareTo(diaActual) < 0) {
                                    orderState = "Delivered"; //Delivered
                                  } else if (diaReserva.compareTo(diaActual) ==
                                      0) {
                                    orderState =
                                        "Ready at: " + horaPickup; //Ready at:
                                  } else {
                                    orderState = "Cancelled"; //Cancelled
                                  }

                                  return Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 8.0, vertical: 2.0),
                                      child: Card(
                                          elevation: 10.0,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(40),
                                          ),
                                          child: ListTile(
                                              leading: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0),
                                                  child: Image.asset(
                                                      "assets/images/borgar.jpg")),
                                              title: Text(res.nombre!),
                                              subtitle: Text(
                                                  "Amount of Plates: " +
                                                      orderList[index]
                                                          .platos
                                                          .length
                                                          .toString()),
                                              trailing: Wrap(
                                                  spacing: 12,
                                                  crossAxisAlignment:
                                                      WrapCrossAlignment.center,
                                                  alignment:
                                                      WrapAlignment.center,
                                                  children: <Widget>[
                                                    Text(orderState)
                                                  ]))));
                                }
                                print(snapshot.hasError);
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Center(
                                      child: CircularProgressIndicator()),
                                );
                              });
                        },
                      ),
                    );
                  }
                  print(snapshot.hasError);
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(child: CircularProgressIndicator()),
                  );
                });
          }
          print(snapshot.hasError);
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(child: CircularProgressIndicator()),
          );
        });
  }
}
