import 'dart:convert';
import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:reservations_app/models/client.dart';
import 'package:reservations_app/models/order.dart';
import 'package:reservations_app/services/order_service.dart';
import '../utils.dart';
import '../widgets/auth/auth_form.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../services/client_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);
  static const routeName = '/auth';

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final _auth = FirebaseAuth.instance;

  bool _fileExists = false;
  bool _fileOrdersExists = false;

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/clientInfo.json');
  }

  Future<File> get _localOrderFile async {
    final path = await _localPath;
    return File('$path/clientOrders.json');
  }

  Future<void> deleteFile(File file) async {
    try {
      if (await file.exists()) {
        await file.delete();
      }
    } catch (e) {
      // Error in getting access to the file.
    }
  }

  var _isLoading = false;
  void _submitAuthForm(
      String email,
      String password,
      String username,
      String phone,
      String documentType,
      String document,
      bool isLogin,
      BuildContext ctx) async {
    UserCredential userCredential;
    try {
      setState(() {
        _isLoading = true;
      });
      final file = await _localFile;
      final fileOrders = await _localOrderFile;
      if (isLogin) {
        userCredential = await _auth.signInWithEmailAndPassword(
            email: email, password: password);

        if (!_fileExists) {
          Client client = await getCurrentClient();
          file.writeAsString(json.encode(client.toJson()));
          _fileExists = true;
          if (!_fileOrdersExists) {
            List<dynamic> orders = await getTtgOrderListByClient(client.id!);
            fileOrders.writeAsString(json.encode(orders));
            _fileOrdersExists = true;
          }
        }

        _sendAnalyticsEventLogin();
      } else {
        userCredential = await _auth.createUserWithEmailAndPassword(
            email: email, password: password);

        Client client = await postClient(Client(
          id: userCredential.user!.uid,
          name: username,
          email: email,
          phone: phone,
          documentType: documentType,
          document: document,
        ));

        deleteFile(file);
        deleteFile(fileOrders);

        file.writeAsString(json.encode(client.toJson()));
        _fileExists = true;

        _sendAnalyticsEventRegister();
      }
    } on PlatformException catch (err) {
      var message = 'An error ocurred, please check your credentials!';
      if (err.message != null) {
        message = err.message!;
      }

      ScaffoldMessenger.of(ctx).showSnackBar(
        SnackBar(
          content: Text(message),
          backgroundColor: Theme.of(ctx).errorColor,
        ),
      );
      setState(() {
        _isLoading = false;
      });
    } on FirebaseAuthException catch (err) {
      var message = 'An error ocurred, please check your credentials!';
      if (err.message != null) {
        message = err.message!;
      }

      ScaffoldMessenger.of(ctx).showSnackBar(
        SnackBar(
          content: Text(message),
          backgroundColor: Theme.of(ctx).errorColor,
        ),
      );
      setState(() {
        _isLoading = false;
      });
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: createMaterialColor(Color(0xffba1200)),
      body: AuthForm(_submitAuthForm, _isLoading),
    );
  }

  Future<void> _sendAnalyticsEventLogin() async {
    FirebaseAnalytics analytics =
        Provider.of<FirebaseAnalytics>(context, listen: false);
    await analytics.logSignUp(signUpMethod: 'email');
  }

  Future<void> _sendAnalyticsEventRegister() async {
    FirebaseAnalytics analytics =
        Provider.of<FirebaseAnalytics>(context, listen: false);
    await analytics.logLogin(loginMethod: 'email');
  }
}
