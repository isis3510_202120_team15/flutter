import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_brand.dart';
import 'package:flutter_credit_card/credit_card_form.dart';
import 'package:flutter_credit_card/credit_card_model.dart';
import 'package:flutter_credit_card/flutter_credit_card.dart';
import 'package:reservations_app/models/client.dart';
import 'package:reservations_app/services/client_service.dart';

// Inspired from https://github.com/SimformSolutionsPvtLtd/flutter_credit_card/blob/master/example/lib/main.dart
class AddCreditCardScreen extends StatefulWidget {
  static const routeName = '/add-credit-card';

  @override
  _AddCreditCardScreenState createState() => _AddCreditCardScreenState();
}

String _translateMessages(String message) {
  switch (message) {
    case 'Nombre no valido':
      return 'Card Holder name not valid';
    case 'Esta tarjeta ya se encuentra registrada':
      return 'Card already registered';
    case 'Tarjeta no valida':
      return 'Number not valid';
    default:
      return message;
  }
}

class _AddCreditCardScreenState extends State<AddCreditCardScreen> {
  String cardNumber = '';
  String expiryDate = '';
  String cardHolderName = '';
  String cvvCode = '';
  bool isCvvFocused = false;
  OutlineInputBorder? border;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final Connectivity _connectivity = Connectivity();
  ConnectivityResult? connectionStatus;

  @override
  void initState() {
    border = OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey.withOpacity(0.7),
        width: 1.0,
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getCurrentClient(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              appBar: AppBar(
                leading: IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  iconSize: 20.0,
                  onPressed: () {
                    Navigator.of(context).pushReplacementNamed('/');
                  },
                ),
                title: const Text('Add credit card'),
              ),
              resizeToAvoidBottomInset: true,
              body: Container(
                child: SafeArea(
                  child: Column(
                    children: <Widget>[
                      const SizedBox(
                        height: 5,
                      ),
                      CreditCardWidget(
                        cardNumber: cardNumber,
                        expiryDate: expiryDate,
                        cardHolderName: cardHolderName,
                        cvvCode: cvvCode,
                        showBackView: isCvvFocused,
                        obscureCardNumber: false,
                        obscureCardCvv: false,
                        isHolderNameVisible: true,
                        cardBgColor: Colors.black,
                        isSwipeGestureEnabled: true,
                        onCreditCardWidgetChange:
                            (CreditCardBrand creditCardBrand) {},
                      ),
                      Expanded(
                        child: SingleChildScrollView(
                          child: Column(
                            children: <Widget>[
                              CreditCardForm(
                                formKey: formKey,
                                obscureCvv: true,
                                obscureNumber: true,
                                cardNumber: cardNumber,
                                cvvCode: cvvCode,
                                isHolderNameVisible: true,
                                isCardNumberVisible: true,
                                isExpiryDateVisible: true,
                                cardHolderName: cardHolderName,
                                expiryDate: expiryDate,
                                themeColor: Colors.blue,
                                textColor: Colors.black,
                                cardNumberDecoration: InputDecoration(
                                  labelText: 'Number',
                                  hintText: 'XXXX XXXX XXXX XXXX',
                                  hintStyle:
                                      const TextStyle(color: Colors.black),
                                  labelStyle:
                                      const TextStyle(color: Colors.black),
                                  focusedBorder: border,
                                  enabledBorder: border,
                                ),
                                expiryDateDecoration: InputDecoration(
                                  hintStyle:
                                      const TextStyle(color: Colors.black),
                                  labelStyle:
                                      const TextStyle(color: Colors.black),
                                  focusedBorder: border,
                                  enabledBorder: border,
                                  labelText: 'Expired Date',
                                  hintText: 'XX/XX',
                                ),
                                cvvCodeDecoration: InputDecoration(
                                  hintStyle:
                                      const TextStyle(color: Colors.black),
                                  labelStyle:
                                      const TextStyle(color: Colors.black),
                                  focusedBorder: border,
                                  enabledBorder: border,
                                  labelText: 'CVV',
                                  hintText: 'XXX',
                                ),
                                cardHolderDecoration: InputDecoration(
                                  hintStyle:
                                      const TextStyle(color: Colors.black),
                                  labelStyle:
                                      const TextStyle(color: Colors.black),
                                  focusedBorder: border,
                                  enabledBorder: border,
                                  labelText: 'Card Holder',
                                ),
                                onCreditCardModelChange:
                                    onCreditCardModelChange,
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8.0),
                                  ),
                                  primary: const Color(0xff1b447b),
                                ),
                                child: Container(
                                  margin: const EdgeInsets.all(12),
                                  child: const Text(
                                    'Add',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: 'halter',
                                      fontSize: 14,
                                      package: 'flutter_credit_card',
                                    ),
                                  ),
                                ),
                                onPressed: () async {
                                  connectionStatus =
                                      await _connectivity.checkConnectivity();
                                  switch (connectionStatus) {
                                    case ConnectivityResult.wifi:
                                    case ConnectivityResult.mobile:
                                      if (formKey.currentState!.validate()) {
                                        var msg = await postCard(
                                            (snapshot.data as Client).id
                                                as String,
                                            cardNumber.replaceAll(" ", ""),
                                            expiryDate.replaceAll("/", "/20"),
                                            cardHolderName);
                                        if (msg != 'created') {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) =>
                                                  AlertDialog(
                                                    title: Text("Attention"),
                                                    content: Text(
                                                        _translateMessages(
                                                            msg)),
                                                    actions: <Widget>[
                                                      TextButton(
                                                        onPressed: () =>
                                                            Navigator.pop(
                                                                context, 'Ok'),
                                                        child: const Text('Ok'),
                                                      ),
                                                    ],
                                                  ));
                                        } else {
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(
                                            SnackBar(
                                              duration:
                                                  const Duration(seconds: 1),
                                              content: Text('Card added'),
                                              backgroundColor:
                                                  Theme.of(context).errorColor,
                                            ),
                                          );
                                          Navigator.of(context)
                                              .pushReplacementNamed('/');
                                        }
                                      }
                                      return;
                                    case ConnectivityResult.none:
                                    case null:
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              AlertDialog(
                                                title: Text("Attention"),
                                                content: Text(
                                                    "You must have internet connection to authenticate"),
                                                actions: <Widget>[
                                                  TextButton(
                                                    onPressed: () =>
                                                        Navigator.pop(
                                                            context, 'Ok'),
                                                    child: const Text('Ok'),
                                                  ),
                                                ],
                                              ));
                                      return;
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(child: CircularProgressIndicator()),
            );
          }
        });
  }

  void onCreditCardModelChange(CreditCardModel? creditCardModel) {
    setState(() {
      cardNumber = creditCardModel!.cardNumber;
      expiryDate = creditCardModel.expiryDate;
      cardHolderName = creditCardModel.cardHolderName;
      cvvCode = creditCardModel.cvvCode;
      isCvvFocused = creditCardModel.isCvvFocused;
    });
  }
}
