import 'package:flutter/material.dart';
import 'package:flutter_credit_card/credit_card_brand.dart';
import 'package:flutter_credit_card/credit_card_widget.dart';
import 'package:reservations_app/models/client.dart';
import 'package:reservations_app/services/card_service.dart';
import 'package:reservations_app/services/client_service.dart';

import '../utils.dart';

class CardsScreen extends StatefulWidget {
  static const routeName = '/credit-cards';
  @override
  State<CardsScreen> createState() => _CardsScreenState();
}

class _CardsScreenState extends State<CardsScreen> {
  bool _isOnline = false;
  late Client currentClient = Client();
  final MyConnectivity _connectivity = MyConnectivity.instance;

  @override
  void initState() {
    super.initState();
    _connectivity.initialize();
    _connectivity.myStream.listen((source) {
      setState(() => _isOnline = source);
    });
    getCurrentClient().then((result) {
      setState(() {
        currentClient = result;
      });
    });
  }


  @override
  void dispose() {
    _connectivity.disposeStream();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: getClientCards(currentClient.id.toString(), _isOnline),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            var cards = snapshot.data as List;
            return Scaffold(
                appBar: AppBar(
                  leading: IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    iconSize: 20.0,
                    onPressed: () {
                      Navigator.of(context).pushReplacementNamed('/');
                    },
                  ),
                  title: const Text('Credit cards saved'),
                ),
                body: ListView.builder(
                    padding: const EdgeInsets.all(8),
                    itemCount: cards.length,
                    itemBuilder: (BuildContext context, int index) {
                      var card = cards[index];
                      return CreditCardWidget(
                        cardNumber: card.cardNumber,
                        expiryDate: card.fecha,
                        cardHolderName: currentClient.name.toString(),
                        cvvCode: "",
                        showBackView: false,
                        onCreditCardWidgetChange:
                            (CreditCardBrand creditCardBrand) {},
                      );
                    }));
          } else {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(child: CircularProgressIndicator()),
            );
          }
        });
  }
}
