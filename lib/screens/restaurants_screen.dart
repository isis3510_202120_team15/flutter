import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:reservations_app/models/client.dart';
import 'package:reservations_app/services/client_service.dart';
import '../utils.dart';
import '../widgets/restaurant_item.dart';
import '../widgets/app_drawer.dart';
import 'package:reservations_app/models/restaurant.dart';
import '../services/restaurant_service.dart';
import 'package:geolocator/geolocator.dart';
import 'package:connectivity/connectivity.dart';

class RestaurantsScreen extends StatefulWidget {
  static const routeName = '/user-products';
  @override
  State<RestaurantsScreen> createState() => _UserProductsScreenState();
}

class _UserProductsScreenState extends State<RestaurantsScreen> {
  bool _isOnline = false;
  final MyConnectivity _connectivity = MyConnectivity.instance;
  bool _ratingCahed = true;
  bool _recommendedCached = true;
  bool _locationsCached = true;
  late Future<dynamic> futureRestaurants;
  bool? _serviceEnabled;
  LocationPermission? _permissionGranted;
  Position? _locationData;
  ConnectivityResult? connectionStatus;

  @override
  void initState() {
    super.initState();
    _connectivity.initialize();
    _connectivity.myStream.listen((source) {
      setState(() => _isOnline = source);
    });
  }

  @override
  void dispose() {
    _connectivity.disposeStream();
    super.dispose();
  }

  Future<dynamic> setLocationVariables(String order) async {
    _serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!_serviceEnabled!) {
      _serviceEnabled = await Geolocator.openLocationSettings();
    }
    _permissionGranted = await Geolocator.checkPermission();
    if (_permissionGranted == LocationPermission.denied) {
      _permissionGranted = await Geolocator.checkPermission();
    }
    _locationData = await Geolocator.getCurrentPosition();
    if (order == 'No')
      return getRestaurants(_isOnline);
    else if (order == 'nearby')
      try {
        return getRestaurantsByposition(
            _locationData!.latitude, _locationData!.longitude, _isOnline);
      } on NoCachedInformation {
        _locationsCached = false;
      }
    else if (order == 'rating')
      try {
        return getRestaurantsByRating(_isOnline);
      } on NoCachedInformation {
        _ratingCahed = false;
      }
    else if (order == 'recommended') {
      Client currentClient = await getCurrentClient();
      try {
        return getRestaurantsByRecommendation(
            currentClient.id as String, _isOnline);
      } on NoCachedInformation {
        _recommendedCached = false;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments;
    void showConnectionDialog() {
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: Text("Attention"),
                content: Text("You must have internet to use this option"),
                actions: <Widget>[
                  TextButton(
                    onPressed: () => Navigator.pop(context, 'Ok'),
                    child: const Text('Ok'),
                  ),
                ],
              ));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
        actions: <Widget>[
          PopupMenuButton(
            icon: const Icon(Icons.filter_list),
            itemBuilder: (context) => [
              PopupMenuItem(
                  child: TextButton(
                child: Text('Sort by rating'),
                onPressed: () {
                  if (_ratingCahed) {
                    Navigator.of(context).pushNamed(RestaurantsScreen.routeName,
                        arguments: {'order': 'rating'});
                    return;
                  }
                  showConnectionDialog();
                },
              )),
              // PopupMenuItem(
              //     child: TextButton(
              //   child: Text('nearby'),
              //   onPressed: () {
              //     if (_locationsCached) {
              //       Navigator.of(context).pushNamed(RestaurantsScreen.routeName,
              //           arguments: {'order': 'nearby'});
              //       return;
              //     }
              //     showConnectionDialog();
              //   },
              // )),
              PopupMenuItem(
                  child: TextButton(
                child: Text('recommended'),
                onPressed: () {
                  if (_recommendedCached) {
                    Navigator.of(context).pushNamed(RestaurantsScreen.routeName,
                        arguments: {'order': 'recommended'});
                    return;
                  }
                  showConnectionDialog();
                },
              )),
            ],
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: FutureBuilder<dynamic>(
        future: arguments != null && (arguments as Map).isNotEmpty
            ? setLocationVariables(arguments['order'])
            : setLocationVariables('No'),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Padding(
              padding: EdgeInsets.all(8),
              child: ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (_, i) => Column(
                  children: [
                    RestaurantItem(
                        snapshot.data[i] as Restaurant,
                        _locationData!.latitude,
                        _locationData!.longitude,
                        _isOnline),
                    Divider(),
                  ],
                ),
              ),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(child: CircularProgressIndicator()),
            );
          }
        },
      ),
    );
  }
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();

  return directory.path;
}

Future<File> get _localFile async {
  final path = await _localPath;
  return File('$path/counter.txt');
}
