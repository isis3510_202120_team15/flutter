import 'dart:convert';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:reservations_app/models/order.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'package:http/http.dart' as http;

const baseUrl = "https://us-central1-ordersapp202120.cloudfunctions.net/";

Future<Order> postOrder(Order order) async {
  return order.puestos != null
      ? postReservationOrder(order)
      : postTtgOrder(order);
}

Future<Order> postTtgOrder(Order order) async {
  print('here');
  final response = await http.post(Uri.parse(baseUrl + "pedido/"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "clienteId": order.clienteId,
        "horaPedido": order.horaReserva,
        "diaPedido": order.diaReserva,
        "platos": order.platos,
        "restauranteId": order.restauranteId,
      }));
  if (response.statusCode == 200) {
    print(response);
    return Order.fromJsonTtg(jsonDecode(response.body));
  } else {
    print(response.body);
    print(response.statusCode);
    print(response.request!.url.data);
    print(response.request!.headers);
    throw Exception("Failed to create order");
  }
}

Future<Order> postReservationOrder(Order order) async {
  print('here');
  final response = await http.post(Uri.parse(baseUrl + "reserva/"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "clienteId": order.clienteId,
        "horaReserva": order.horaReserva,
        "diaReserva": order.diaReserva,
        "platos": order.platos,
        "puestos": order.puestos,
        "restauranteId": order.restauranteId,
      }));
  if (response.statusCode == 200) {
    print(response);
    return Order.fromJson(jsonDecode(response.body));
  } else {
    print(response.body);
    print(response.statusCode);
    print(response.request!.url.data);
    print(response.request!.headers);
    throw Exception("Failed to create order");
  }
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path;
}

Future<File> get _localFile async {
  final path = await _localPath;
  return File('$path/clientOrders.json');
}

Future<List> getTtgOrderListByClient(String clienteId) async {
  var orders = [];
  var data;

  bool _fileExists = false;
  File _filePath;

  _filePath = await _localFile;

  _fileExists = await _filePath.exists();

  if (_fileExists) {
    String content = await _filePath.readAsString();
    print(content);
    for (var order in jsonDecode(content)) {
      // orders.add(Order.fromJsonTtg(order));
      print(order);
    }

    final response =
        await http.get(Uri.parse(baseUrl + "pedido?cliente=" + clienteId));
    if (response.statusCode == 200) {
      data = response.body;
      for (var order in jsonDecode(data)) {
        orders.add(Order.fromJsonTtg(order));
      }
      return orders;
    } else {
      print(response.statusCode);
      throw Exception("Failed to fetch TTG orders from client");
    }
  } else {
    if (!_fileExists) {
      List<dynamic> orders = await getTtgOrderListByClient(clienteId);
      _filePath.writeAsString(json.encode(orders));
      _fileExists = true;
    }

    String content = await _filePath.readAsString();
    for (var order in jsonDecode(content)) {
      orders.add(Order.fromJsonTtg(order));
      print(Order.fromJsonTtg(order));
    }
    return orders;
  }
}
