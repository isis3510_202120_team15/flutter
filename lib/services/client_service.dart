import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:reservations_app/models/client.dart';
import 'package:cached_network_image/cached_network_image.dart';

const baseUrl = "https://us-central1-ordersapp202120.cloudfunctions.net/";

Future<Client> getClientById(String id) async {
  final response = await http.get(Uri.parse(baseUrl + "cliente/" + id));
  if (response.statusCode == 200) {
    return Client.fromJson(jsonDecode(response.body));
  } else {
    print(response.statusCode);
    throw Exception("Failed to fetch client");
  }
}

Future<Client> getClientByEmail(String email) async {
  final response =
      await http.get(Uri.parse(baseUrl + "cliente?email=" + email));
  if (response.statusCode == 200) {
    return Client.fromJson(jsonDecode(response.body));
  } else {
    print(response.statusCode);
    throw Exception("Failed to fetch client");
  }
}

Future<Client> postClient(Client client) async {
  print('here');
  final response = await http.post(Uri.parse(baseUrl + "cliente/"),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "correo": client.email,
        "documento": client.document,
        "foto":
            "https://d25rq8gxcq0p71.cloudfront.net/dictionary-images/324/419665d2-74b7-49d4-b3c8-3aea253f966f.jpg",
        "id": client.id,
        "nombre": client.name,
        "tarjetas": [],
        "pedidos": [],
        "telefono": client.phone,
        "tipoDoc": client.documentType
      }));
  if (response.statusCode == 200) {
    print(response);
    return Client.fromJson(jsonDecode(response.body));
  } else {
    print(response.body);
    print(response.statusCode);
    print(response.request!.url.data);
    print(response.request!.headers);
    throw Exception("Failed to create client");
  }
}

Future<String> get _localPath async {
  final directory = await getApplicationDocumentsDirectory();
  return directory.path;
}

Future<File> get _localFile async {
  final path = await _localPath;
  return File('$path/clientInfo.json');
}

Future<dynamic> getCurrentClient() async {
  Future client;

  bool _fileExists = false;
  File _filePath;

  _filePath = await _localFile;

  _fileExists = await _filePath.exists();

  if (_fileExists) {
    String content = await _filePath.readAsString();
    print(Client.fromJson(jsonDecode(content)).name);
    return Client.fromJson(jsonDecode(content));
  } else {
    final FirebaseAuth auth = FirebaseAuth.instance;
    User? user = auth.currentUser;
    client = getClientByEmail(user!.email as String);
    return client;
  }
}

Future<dynamic> postCard(String clientId, String cardNumber, String cardExpiry,
    String cardHolder) async {
  final response = await http.post(
      Uri.parse(baseUrl + "cliente/" + clientId + '/tarjeta'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, dynamic>{
        "cardHolder": cardHolder,
        "cardNumber": cardNumber,
        "fecha": cardExpiry
      }));
  if (response.statusCode == 200) {
    return 'created';
  } else if (response.statusCode == 400) {
    return response.body;
  } else {
    throw Exception("Failed to create card");
  }
}
