import 'dart:convert';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:http/http.dart' as http;
import 'package:reservations_app/models/card.dart';

const baseUrl = "https://us-central1-ordersapp202120.cloudfunctions.net/";

Future<List> getClientCards(String id, bool isOnline) async {
  var cards = [];
  var data;
  if (!isOnline) {
    var file = await DefaultCacheManager().getFileFromCache("cards");
    var fetchedFile = file!.file;
    await fetchedFile
        .readAsString()
        .then((String contents) => {data = contents});
  } else {
    final response = await http.get(Uri.parse("${baseUrl}cliente/${id}/tarjeta"));
    if (response.statusCode == 200) {
      DefaultCacheManager().putFile("${baseUrl}cliente/${id}/tarjeta", response.bodyBytes,
          fileExtension: "json", key: "cards");
      data = response.body;
    } else {
      throw Exception("Failed to fetch cards");
    }
  }
  for (var card in jsonDecode(data)) {
    cards.add(Card.fromJson(card));
  }
  return cards;
}
