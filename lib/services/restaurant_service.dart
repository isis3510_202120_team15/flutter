import 'dart:convert';

import '../utils.dart';
import '/models/restaurant.dart';
import 'package:http/http.dart' as http;
import 'dart:math' show cos, sqrt, asin;
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

const baseUrl = "https://us-central1-ordersapp202120.cloudfunctions.net/";
const googleUrl = "https://maps.googleapis.com/maps/api/";
const googleApiKey = "AIzaSyDboni-N2NAnMAQIlFbPBpAKdP-4Rxz4jE";

Future<List> getRestaurants(bool isOnline) async {
  var rests = [];
  var data;
  if (!isOnline) {
    var file = await DefaultCacheManager().getFileFromCache("restaurants");
    var fetchedFile = file!.file;
    await fetchedFile
        .readAsString()
        .then((String contents) => {data = contents});
  } else {
    final response = await http.get(Uri.parse(baseUrl + "restaurante"));
    if (response.statusCode == 200) {
      DefaultCacheManager().putFile(baseUrl + "restaurante", response.bodyBytes,
          fileExtension: "json", key: "restaurants");
      data = response.body;
    } else {
      throw Exception("Failed to fetch restaurant");
    }
  }
  for (var restaurant in jsonDecode(data)) {
    rests.add(Restaurant.fromJson(restaurant));
  }
  return rests;
}

int compareDistance(String a, String b) {
  double disA = a == "" ? 0 : double.parse(a.replaceAll("km", ""));
  double disB = b == "" ? 0 : double.parse(b.replaceAll("km", ""));
  print(disA);
  print(disB);
  if (disA < disB) {
    return -1;
  } else {
    return 1;
  }
}

Future<List> getRestaurantsByposition(
    double latitude, double longitude, bool isOnline) async {
  var rests = [];
  var data;
  if (!isOnline) {
    var file = await DefaultCacheManager().getFileFromCache("restaurants");
    if (file != null) {
      var fetchedFile = file.file;
      await fetchedFile
          .readAsString()
          .then((String contents) => {data = contents});
    } else {
      throw new NoCachedInformation(
          "No information found for restaurants_rating");
    }
  } else {
    final response = await http.get(Uri.parse(baseUrl + "restaurante"));
    if (response.statusCode == 200) {
      data = response.body;
    } else {
      throw Exception("Failed to fetch restaurant");
    }
  }
  for (var restaurant in jsonDecode(data)) {
    Restaurant rest = Restaurant.fromJson(restaurant);
    dynamic locations =
        await getRestaurantLocation(rest, latitude, longitude, isOnline);
    if (locations != null) {
      rest.distance = locations[2];
    } else {
      rest.distance = "";
    }
    rests.add(rest);
    rests.sort((a, b) => compareDistance(a.distance, b.distance));
  }
  return rests;
}

Future<List> getRestaurantsByRating(bool isOnline) async {
  var rests = [];
  var data;
  if (!isOnline) {
    var file =
        await DefaultCacheManager().getFileFromCache("restaurants_rating");
    if (file != null) {
      var fetchedFile = file.file;
      await fetchedFile
          .readAsString()
          .then((String contents) => {data = contents});
    } else {
      throw new NoCachedInformation(
          "No information found for restaurants_rating");
    }
  } else {
    final response =
        await http.get(Uri.parse("${baseUrl}restaurante?order=rating"));
    if (response.statusCode == 200) {
      DefaultCacheManager().putFile(
          "${baseUrl}restaurante?order=rating", response.bodyBytes,
          fileExtension: "json", key: "restaurants_rating");
      data = response.body;
    } else {
      throw Exception("Failed to fetch restaurant");
    }
  }
  for (var restaurant in jsonDecode(data)) {
    rests.add(Restaurant.fromJson(restaurant));
  }
  return rests;
}

Future<List> getRestaurantsByRecommendation(
    String userId, bool isOnline) async {
  var rests = [];
  var data;
  if (!isOnline) {
    var file = await DefaultCacheManager()
        .getFileFromCache("restaurants_recommendation");
    if (file != null) {
      var fetchedFile = file.file;
      await fetchedFile
          .readAsString()
          .then((String contents) => {data = contents});
    } else {
      throw new NoCachedInformation(
          "No information found for restaurants_recommendation");
    }
  } else {
    final response =
        await http.get(Uri.parse("${baseUrl}recomendaciones/$userId"));
    if (response.statusCode == 200) {
      DefaultCacheManager().putFile(
          "${baseUrl}recomendaciones/$userId", response.bodyBytes,
          fileExtension: "json", key: "restaurants_recommendation");
      data = response.body;
    } else {
      throw Exception("Failed to fetch restaurant");
    }
  }
  for (var restaurant in jsonDecode(data)) {
    rests.add(Restaurant.fromJson(restaurant));
  }
  return rests;
}

dynamic getRestaurantLocation(Restaurant restaurant, double ilatitude,
    double ilongitude, bool isOnline) async {
  var data;
  print("Cosa: " + isOnline.toString());
  String address = (restaurant.direccion as String)
      .replaceAll(" ", "%20")
      .replaceAll("#", "%23");
  if (!isOnline) {
    var file =
        await DefaultCacheManager().getFileFromCache("restaurant_$address");
    if (file != null) {
      var fetchedFile = file.file;
      await fetchedFile
          .readAsString()
          .then((String contents) => {data = contents});
    } else {
      throw new NoCachedInformation("No information found for $address");
    }
  } else {
    final response = await http.get(Uri.parse(
        "${googleUrl}geocode/json?address=$address%20Bogotá&key=$googleApiKey"));
    if (response.statusCode == 200) {
      DefaultCacheManager().putFile(
          "${googleUrl}geocode/json?address=$address%20Bogotá&key=$googleApiKey",
          response.bodyBytes,
          fileExtension: "json",
          key: "restaurant_$address");
      data = response.body;
    } else {
      throw Exception("Failed to fetch Coordinates");
    }
  }
  var res = [];
  data = jsonDecode(data);
  var latitude = data["results"][0]["geometry"]["location"]["lat"];
  var longitude = data["results"][0]["geometry"]["location"]["lng"];
  String distance = await getDistanceBetweenPoints(
      ilatitude, ilongitude, latitude, longitude, isOnline);
  res.add(latitude);
  res.add(longitude);
  res.add(distance);
  return res;
}

Future<Restaurant> getRestaurantById(String id) async {
  final response = await http.get(Uri.parse(baseUrl + "restaurante/" + id));
  if (response.statusCode == 200) {
    return Restaurant.fromJson(jsonDecode(response.body));
  } else {
    print(response.statusCode);
    throw Exception("Failed to fetch restaurant");
  }
}

Future<String> getDistanceBetweenPoints(
    fromLat, fromLon, toLat, toLon, bool isOnline) async {
  var data;
  if (!isOnline) {
    var file = await DefaultCacheManager()
        .getFileFromCache("$fromLat-$fromLon-$toLat-$toLon");
    if (file != null) {
      var fetchedFile = file.file;
      await fetchedFile
          .readAsString()
          .then((String contents) => {data = contents});
    } else {
      throw new NoCachedInformation("No information found for coordinates");
    }
  } else {
    String coordinates =
        "origins=$fromLat%2C$fromLon&destinations=$toLat%2C$toLon";
    final response = await http.get(Uri.parse(
        "${googleUrl}distancematrix/json?$coordinates&key=$googleApiKey"));
    if (response.statusCode == 200) {
      DefaultCacheManager().putFile(
          "${googleUrl}distancematrix/json?$coordinates&key=$googleApiKey",
          response.bodyBytes,
          fileExtension: "json",
          key: "$fromLat-$fromLon-$toLat-$toLon");
      data = response.body;
    } else {
      throw Exception("Failed to fetch Coordinates");
    }
  }
  data = jsonDecode(data);
  String status = data["rows"][0]["elements"][0]["status"];
  if (status == "ZERO_RESULTS") {
    return "";
  }
  String distance = data["rows"][0]["elements"][0]["distance"]["text"];
  return distance;
}
