import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:reservations_app/models/client.dart';
import 'package:reservations_app/models/plate.dart';
import 'package:reservations_app/services/client_service.dart';
import 'package:reservations_app/services/order_service.dart';
import 'package:reservations_app/widgets/my_stateful_text.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:connectivity/connectivity.dart';

import 'models/order.dart';

class Cart extends StatefulWidget {
  static const routeName = '/cart-detail';
  final String selectedReservationHour;
  final Map cartList;
  final ValueChanged addCount;
  final ValueChanged substractCount;
  final String restauranteId;
  final int? cantPuestos;

  const Cart(
      {Key? key,
      required this.selectedReservationHour,
      required this.cartList,
      required this.addCount,
      required this.substractCount,
      required this.restauranteId,
      required this.cantPuestos})
      : super(key: key);

  @override
  _CartDetail createState() => _CartDetail();
}

class _CartDetail extends State<Cart> {
  List<int> objValue = [];
  final FirebaseAuth auth = FirebaseAuth.instance;

  Future<dynamic> getClientId() async {
    // User? user = auth.currentUser;
    Future client = getCurrentClient();
    // Future client = getClientByEmail(user!.email as String);
    return client;
  }

  @override
  Widget build(BuildContext context) {
    var itemsKey = widget.cartList.keys;
    var itemsValue = widget.cartList.values;
    final Connectivity _connectivity = Connectivity();
    ConnectivityResult? connectionStatus;
    double paymentTotal = 0;
    // double paymentTemp = 0;
    List<Plate> objKey = [];
    List<String> idPlates = [];

    for (var item in itemsKey) {
      objKey.add(item);
    }

    for (var item in itemsValue) {
      objValue.add(item);
    }

    for (var i = 0; i < objKey.length; i++) {
      paymentTotal += objKey[i].precio! * objValue[i];
    }

    return FutureBuilder(
        future: getClientId(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            Client client = snapshot.data as Client;

            return Scaffold(
                appBar: AppBar(
                  title: const Text("Cart"),
                ),
                body: ListView.builder(
                    itemCount: widget.cartList.length,
                    itemBuilder: (context, index) {
                      if (widget.cartList.isEmpty) {
                        return Text(
                            "There are no items in the cart, go back to the restaurant's to pick your favourite plates");
                      }

                      return Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8.0, vertical: 2.0),
                        child: Card(
                          elevation: 10.0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40),
                          ),
                          child: ListTile(
                              leading: ClipRRect(
                                  borderRadius: BorderRadius.circular(20.0),
                                  child:
                                      Image.asset("assets/images/borgar.jpg")),
                              title: Text(objKey[index].nombre!),
                              subtitle: Text(objKey[index].precio.toString()),
                              trailing: Wrap(
                                  spacing: 12,
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  alignment: WrapAlignment.center,
                                  children: <Widget>[
                                    GestureDetector(
                                      child: Icon(
                                        Icons.remove_circle,
                                        color: Colors.red,
                                        size: 30,
                                      ),
                                      onTap: () {
                                        if (objValue[index] <= 1) {
                                          setState(() {
                                            widget.cartList
                                                .remove(objKey[index]);
                                            // paymentTotal -= objKey[index].precio;
                                          });
                                        } else {
                                          setState(() {
                                            objValue[index] =
                                                objValue[index] - 1;
                                            // paymentTotal -= objKey[index].precio;
                                          });
                                        }
                                        setState(() {
                                          widget.substractCount(objKey[index]);
                                        });
                                        // Remove item from cart and update restaurant detail screen
                                      },
                                    ),
                                    MyStatefulText(
                                        myValue: objValue[index].toString(),
                                        key: UniqueKey()),
                                    GestureDetector(
                                      child: Icon(
                                        Icons.add_circle,
                                        color: Color(0xFF031927),
                                        size: 30,
                                      ),
                                      onTap: () {
                                        setState(() {
                                          objValue[index] = objValue[index] + 1;
                                          // paymentTotal += objKey[index].precio;
                                          widget.addCount(objKey[index]);
                                        });
                                        // Add item from cart and update restaurant detail screen
                                      },
                                    ),
                                  ])),
                        ),
                      );
                    }),
                bottomNavigationBar: BottomAppBar(
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.9,
                            child: FloatingActionButton.extended(
                              onPressed: () async {
                                for (var i = 0; i < objKey.length; i++) {
                                  for (var j = 0; j < objValue[i]; j++) {
                                    idPlates.add(objKey[i].id!);
                                  }
                                }

                                DateTime now = new DateTime.now();

                                var formatterDay = new DateFormat('dd/MM/yyyy');
                                //var formatterHour = new DateFormat('HH:mm');

                                String? clienteId = client.id;
                                String diaReserva = formatterDay.format(now);
                                // String horaPedida = formatterHour.format(now);
                                Order order = Order(
                                    clienteId,
                                    widget.restauranteId,
                                    idPlates,
                                    diaReserva,
                                    widget.selectedReservationHour,
                                    widget.cantPuestos);
                                print("order: " +
                                    order.clienteId! +
                                    " restauranteID: " +
                                    order.restauranteId! +
                                    " idPlates: " +
                                    order.platos!.length.toString() +
                                    " diaReserva: " +
                                    order.diaReserva! +
                                    " selectedResHour: " +
                                    order.diaReserva!);
                                connectionStatus =
                                    await _connectivity.checkConnectivity();
                                switch (connectionStatus) {
                                  case ConnectivityResult.wifi:
                                    postOrder(order);
                                    return;
                                  case ConnectivityResult.mobile:
                                    postOrder(order);
                                    print("order sent");

                                    return;
                                  case ConnectivityResult.none:
                                  case null:
                                    showDialog(
                                        context: context,
                                        builder: (BuildContext context) =>
                                            AlertDialog(
                                              title: Text("Attention"),
                                              content: Text(
                                                  "You must have internet connection to reserve"),
                                              actions: <Widget>[
                                                TextButton(
                                                  onPressed: () =>
                                                      Navigator.pop(
                                                          context, 'Ok'),
                                                  child: const Text('Ok'),
                                                ),
                                              ],
                                            ));
                                    return;
                                }
                              },
                              label: Wrap(
                                spacing: 30,
                                children: [
                                  Text('Pay'),
                                  Text("Total: \$" + paymentTotal.toString())
                                ],
                              ),
                              icon: const Icon(Icons.payment),
                              backgroundColor: Color(0xffba1200),
                            ),
                          ))
                    ],
                  ),
                ));
          }
          print(snapshot.hasError);
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(child: CircularProgressIndicator()),
          );
        });
  }
}
